#Pilar Hernández Pacheco


def probar_condicion_fila_columna(lista, val): #se define la funcion que detecta si el numero que se quiere usar esta en la fila o en la columna
    in_lista = val in lista
    return in_lista

def probar_condicion_miniblock(fil_block, col_block ,val , x): #se define la funcion que detecta si el numero que se quiere usar se encuentra en el bloque (se llaman bloques a las mini matrices de 3x3)
    for i in range(fil_block,fil_block +3):
        for j in range(col_block, col_block+3):
            if x[i][j] == val:
                return True
    return False

def encontrar_valor(x, fil, col): 
    for val in range(1,10,1):#valores que pueden ir en el sudoku
        prueba1=probar_condicion_fila_columna(x[fil],val)  #realiza chequeo 1, si el valor a probar se encuentra en la fila
        if(prueba1==True):
            continue
        columna=[]
        for i in range(9):  ##con este for recorremos las filas y agregamos al vector columna cada indice de columna
            fila_actual=x[i]
            columna.append(fila_actual[col])
        prueba2=probar_condicion_fila_columna(columna,val) #realizar chequeo 2, si el valor a probar se encuentra en la columna
        if (prueba2==True):
            continue
        prueba3=probar_condicion_miniblock((fil//3 ) * 3, (col//3)*3 , val, x) #realiza chequeo 3, si el valor a probar se encuentra en el bloque (mini matriz de 3x3)
        if(prueba3==True):
            continue
        x[fil][col]=val
        if(__main__(x)==1): # si el puzzle esta resuelto entonces se sale de la funcion
            return 1

        x[fil][col]=0
    
    return 0

#se define el mapa del sudoku como una matriz de 9x9, donde cda fila es una lista.
x=     [[5,3,0,0,7,0,0,0,0],
        [6,0,0,1,9,5,0,0,0],
        [0,9,8,0,0,0,0,6,0],
        [8,0,0,0,6,0,0,0,3],
        [4,0,0,8,0,3,0,0,1],
        [7,0,0,0,2,0,0,0,6],
        [0,6,0,0,0,0,2,8,0],
        [0,0,0,4,1,9,0,0,5],
        [0,0,0,0,8,0,0,7,9],]

def __main__(x):
    for fila in range(0,9): #este for toma la fila
        for columna in range(0,9): #este for toma el indice de cada fila
            if  x[fila][columna]==0:  # indices donde hay 0
                if(encontrar_valor(x,fila,columna)==0):  #si no encontró solucion valida, retorna 0
                    return 0        
    return 1  ##si ya no quedan filas y columnas sin rellenar, retorna 1

__main__(x)
print("Sudoku resuelto!!!!")
print(x)